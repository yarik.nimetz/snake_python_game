from tkinter import *
import random
import time

class Snake:
    def __init__(self,canvas,block_info):
        self.canvas = canvas
        self.y = self.x = 0
        self.i = -1
        self.parts = []
        self.coords_part = []
        self.size = block_info['size_part']
        self.color_food = block_info["color_food"]
        new_part = self.canvas.create_rectangle(0, 0, self.size, self.size,fill=block_info["color_part"])
        self.food = self.canvas.create_rectangle(0, 0, self.size, self.size,fill=self.color_food)
        self.parts.append(new_part)
        self.canvas.move(self.parts[0],250,300)
        self.width = (random.randrange(1,(self.canvas.winfo_reqwidth()-self.size)//self.size)*self.size)
        self.height= (random.randrange(1,(self.canvas.winfo_reqheight()-self.size)//self.size)*self.size)
        self.canvas.move(self.food,self.width,self.height)
        canvas.bind_all('<KeyPress>', self.direction)

    def collision_block(self,first_block,second_block):
        return self.canvas.coords(first_block)[0] == self.canvas.coords(second_block)[0] and self.canvas.coords(first_block)[1] == self.canvas.coords(second_block)[1]

    def direction(self,event):
        evn = event.keysym
        if evn == "Up" and self.y != self.size:
            self.y = -1 * self.size
            self.x = 0
        if evn == "Down" and self.y != -1 * self.size:
            self.y = self.size
            self.x = 0 
        if evn == "Right" and self.x != -1*self.size:
            self.y = 0
            self.x = self.size
        if evn == "Left" and self.x != self.size:
            self.y = 0
            self.x = -1*self.size
        
    def collision_snake_foods(self):
            if snake1.collision_block(self.parts[0],self.food):
                self.canvas.move(self.food,self.width*-1,self.height*-1)
                self.width = (random.randrange(1,(self.canvas.winfo_reqwidth()-self.size)//self.size)*self.size)
                self.height= (random.randrange(1,(self.canvas.winfo_reqheight()-self.size)//self.size)*self.size)
                self.canvas.move(self.food,self.width,self.height)
                return True
            return False
    
    def snake_move(self):
        if snake1.collision_snake_foods():
            new_part = self.canvas.create_rectangle(0, 0, self.size, self.size,fill='yellow')
            self.canvas.move(new_part,self.last_part[0],self.last_part[1])
            self.parts.append(new_part)
        if self.x != 0 or self.y != 0:
            self.i = self.i + 2
            self.coords_part.append(self.x)
            self.coords_part.append(self.y)
            self.last_part = (self.canvas.coords(self.parts[-1])[0],self.canvas.coords(self.parts[-1])[1])
        self.canvas.move(self.parts[0],self.x,self.y)
        for u in range(1,len(self.parts),1):
            x = (self.i-1)-u*2
            y = self.i-u*2
            self.canvas.move(self.parts[u],self.coords_part[x],self.coords_part[y])
        
    def collision_borders(self):
        if self.canvas.coords(self.parts[0])[0] <= 0 or self.canvas.coords(self.parts[0])[1] <= 0\
            or self.canvas.coords(self.parts[0])[0] >= (self.canvas.winfo_reqwidth()-self.size) or \
                self.canvas.coords(self.parts[0])[1] >= (self.canvas.winfo_reqheight()-self.size): 
            return len(self.parts)
        
        for u in range(1,len(self.parts),1):
            if snake1.collision_block(self.parts[0],self.parts[u]):
                return len(self.parts)
        return 0
    
    def tick(self):
        if not self.collision_borders():
                self.snake_move()
        else:
            self.canvas.create_text(canvas_width/2,0.44 * canvas_height, text = 'Game Over',fill='red',font=('Helvetica',50))
            self.canvas.create_text(canvas_width/2,0.52 * canvas_height, text = 'Your Score %s' % (int(snake1.collision_borders())-1),fill='red',font=('Helvetica',20))

    def run(self):
        while True:
            self.tick()
            tk.update()
            time.sleep(0.025)
        


tk = Tk()
tk.title('Gra')
tk.resizable(0,0)
tk.wm_attributes("-topmost", 1)
canvas_width = 700
canvas_height = 600
canvas = Canvas(tk, width = canvas_width, height = canvas_height, bd=0, highlightthickness=0)
canvas.pack()
block_info = {'size_part':10,
              "color_part":'yellow',
              "color_food":"red"}

snake1 = Snake(canvas,block_info)
snake1.run()